using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace V_mayor_y_menor
{
    // Crear un programa que permita crear, 
    //cargar y obtener el menor y mayor valor de un vector. 
    //La obtención del mayor y menor hacerlo en un único 
    //método que retorne dichos dos valores.
    
    class Program
    {
        public static int[] MyN()
        {
            Console.WriteLine("cuantos valores ingresara");
            int c = int.Parse(Console.ReadLine());
            int[] vector = new int[c];

            for (int i = 0; i < vector.Length; i++)
            {
                Console.Write("El valor " + i+1 + " sera igual a: ");
                vector[i] = int.Parse(Console.ReadLine());
            }

            int[] MinMax = new int[2];

            MinMax[0] = vector.Min();
            MinMax[1] = vector.Max();

            Console.WriteLine("El valor menor es igual a " + MinMax[0]);
            Console.WriteLine("El valor mayor es igual a " + MinMax[1]);

            return MinMax;
        }
        static void Main(string[] args)
        {
            MyN();

            Console.ReadKey();
        }
    }
}
