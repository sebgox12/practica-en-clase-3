﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace valoresR
{
    class Program
    {
        //// Crear un metodo que retorne 5 valores random entre 1 y 30 mediante parametros por referencia.
        class R
        {
            public void valor(ref int num, ref int num2)
            {
                Random valor = new Random();
                for(int i = 1; i <= 5; i++)
                {
                    float VMostrar = valor.Next(num, num2);
                    Console.WriteLine("valor " + i + " es igual a " + VMostrar);

                }
                
            }
        }
        static void Main(string[] args)
        {
            int numero = 1;
            int numero2 = 30;

            R mostrar = new R();
            mostrar.valor(ref numero,ref numero2);

            Console.ReadKey();
        }
    }
}
